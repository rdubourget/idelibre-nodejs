// var redis = require('redis');
// var client = redis.createClient();
//
// client.on('error', function(err){
//     console.log('Something went wrong ', err)
// });
//
// client.set('my test key', 'my test value', redis.print);
// client.get('my test key', function(error, result) {
//     if (error) throw error;
//     console.log('GET result ->', result)
// });



// const fs = require('fs')
// const testFile = fs.readFileSync('test.txt', 'utf8')
// const regex = /^([0-9]+$)/gm
// let results = testFile.match(regex)
// console.log(results)


//
// const regex = /\b(0?[1-9]|[12]\d|3[01])([ \/\-])(0?[1-9]|1[012])\2(\d{4})/
// const str = `Today's date is 18/09/2017`
// const subst = `$3$2$1$2$4`
// // const subst = `$3-$1-$4`
//  const result = str.replace(regex, subst)
// //let results = str.match(regex)
// console.log(result)


// function isValidEmail (input) {
//     const regex = /^[^@\s]+@[^@\s]+\.\w{2,6}$/;
//     const result = regex.exec(input)
//
//     // If result is null, no match was found
//     return !!result
// }
//
// const tests = [
//     `test.test@gmail.com`, // Valid
//     '', // Invalid
//     `test.test`, // Invalid
//     '@invalid@test.com', // Invalid
//     'invalid@@test.com', // Invalid
//     `gmail.com`, // Invalid
//     `this is a test@test.com`, // Invalid
//     `test.test@gmail.comtest.test@gmail.com` // Invalid
// ]
//
// console.log(tests.map(isValidEmail))







var pg = require('pg');
var squel = require("squel");



var redis = require('redis');
var client = redis.createClient();




function flushAll(){
    client.flushdb( function (err, succeeded) {
        console.log(succeeded); // will be true if successfull
    });
}


function saveToRedis(arrayToInsert, uuid) {
    var multi = client.multi();

    arrayToInsert.forEach(function (el) {
        console.log(el);
        multi.set(uuid + ":" +el.id, el.path)
    });

    multi.exec(function (err, results) {
        console.log(err),
            console.log(results);
    });
}


function getIdPathAndCopy(conn, uuid) {

    var request = squel.select()
        .from("documents")
        .field("path")
        .field("id")
        .toString();

//console.log('request %s', request);
    pg.connect(conn, function (err, client, done) {
        if (err) {
            console.log('error fetching client from pool %s', err);
            console.log(err);
        }
        client.query(request, function (err, result) {
            done();
            if (err) {
                console.log(err);
            }

            // console.log(result.rows.map(r => r.id));
            // return;
            saveToRedis(result.rows, uuid);
        });
    });

}


 //flushAll();

 // var conn = "postgres:idelibre:idelibre@localhost:5432/idelibre_libriciel"
 // getIdPathAndCopy(conn, "5810a34f-c540-4ad1-9caa-189c5cde535b");
//
// console.log(`test de string avec uuid : ${conn}`);


var tab  =[ '59f8a1c5-469c-4508-a584-13805cde535b',
    '59f2ea4d-cc70-4a77-8873-5b545cde535b',
    '59fc9067-78f8-42cf-832c-19545cde535b',
    '59f8a1c5-9284-4548-a7a3-13805cde535b',
    '59f8a1c5-fff8-4b1b-a0e9-13805cde535b',
    '59f8a1c5-0170-48f2-958e-13805cde535b',
    '59f2ea71-a904-48a5-a7f8-5b545cde535b',
    '59f8a1c5-5b20-4234-b9e6-13805cde535b',
    '59f8a1c5-2b20-474d-aec8-13805cde535b',
    '59f8a1c5-73c0-4a76-924b-13805cde535b',
    '59f8a1c5-a4c0-450a-9fd4-13805cde535b',
    '59f32af9-6084-43be-9f3b-77c05cde535b',
    '59fc9067-4eb0-4192-b817-19545cde535b',
    '59f6f8f9-9eac-4f99-8b73-45a95cde535b',
    '59f6f8f9-2d7c-4cbf-823a-45a95cde535b',
    '59fc9067-d710-4986-bb03-19545cde535b',
    '59f6f8f9-e5c8-45ef-ae87-45a95cde535b',
    '59f6f8f9-3944-4aa6-adb1-45a95cde535b',
    '5a1e9332-0d94-4d05-9c17-6b775cde535b',
    '5a129ccd-dadc-429f-b6ac-45505cde535b',
    '5a129ccd-2424-4810-9a31-45505cde535b',
    '5a1bda10-fabc-4f75-8b09-6b775cde535b',
    '5a0c0701-48c4-4687-a8d8-041a5cde535b',
    '5a0c0701-0ee4-4b55-a4eb-041a5cde535b',
    '5a0c0701-80e4-441b-b20c-041a5cde535b',
    '5a0c0701-4098-4ef7-aa2c-041a5cde535b',
    '5a0c0702-847c-4735-b1f9-041a5cde535b',
    '5a0c0702-44dc-4d07-86f3-041a5cde535b',
    '5a0c0702-3d3c-43c8-a8dc-041a5cde535b',
    '5a0c0701-cbe0-4344-b917-041a5cde535b',
    '5a1bda10-8710-48b0-86d1-6b775cde535b',
    '5a12d3a9-a900-455d-97c6-53b55cde535b',
    '5a12d3a9-df7c-4c90-b416-53b55cde535b',
    '5a12f913-d54c-4539-8eb1-45505cde535b',
    '5a12f913-a704-4fa6-bcca-45505cde535b',
    '5a12f913-16c4-4a42-a5d8-45505cde535b',
    '5a1e9332-800c-4b1a-a91f-6b775cde535b',
    '5a1e9332-a900-4b04-b77f-6b775cde535b',
    '5a1e9332-54b4-4903-be1b-6b775cde535b',
    '5a1e9332-8ea0-4814-acae-6b775cde535b',
    '5a1e9332-54f0-4ee3-854b-6b775cde535b',
    '5a1e9332-bf28-491c-aa2d-6b775cde535b',
    '5a1e9332-e9fc-4eaa-a3aa-6b775cde535b',
    '5a2028a4-562c-4c7b-a8d6-043066d7706e',
    '5a2027db-0b60-4ca8-ba6e-042f66d7706e',
    '5a2027db-1094-4ac1-beb0-042f66d7706e',
    '5a2027db-0d94-488b-8ec2-042f66d7706e',
    '5a2027e4-f760-4110-a433-043066d7706e',
    '5a2027e4-c44c-4b18-ab1b-043066d7706e',
    '5a2027e4-09f0-40a6-a1fa-043066d7706e',
    '5a2027fa-8588-41f0-99be-042f66d7706e',
    '5a2027fa-1454-4466-8db5-042f66d7706e',
    '5a2027fa-bfdc-45b5-8b0f-042f66d7706e',
    '5a20280c-2a50-41ce-902a-043066d7706e',
    '5a20280c-bf54-4354-ae29-043066d7706e',
    '5a20280c-db80-4bac-8494-043066d7706e',
    '5a202895-4ef4-431d-a914-042f66d7706e',
    '5a202895-ec08-4881-83da-042f66d7706e',
    '5a202895-2d10-444d-b21d-042f66d7706e',
    '5a2028a4-a60c-422d-8bf0-043066d7706e',
    '5a2028a4-0e1c-41d1-95ef-043066d7706e',
    '5a202a81-c2c4-44e2-a689-043066d7706e',
    '5a4ddb32-0ee8-4ee5-b3c6-04d566d7706e',
    '5a4ddb32-e584-4d93-b62e-04d566d7706e',
    '5a5489db-49c4-44b4-a27b-321366d7706e',
    '5a4ddb4b-3d3c-4d3d-9d37-04d566d7706e',
    '5a4ddb4b-c5b8-4738-9639-04d566d7706e',
    '5a4ddc60-91c8-4c07-95a6-04d466d7706e',
    '5a4ddc60-fd00-4c3f-97e8-04d466d7706e',
    '5a576fb2-3c20-4869-9b5e-052166d7706e',
    '5a4ddcf7-0964-4671-a8fc-04d566d7706e',
    '5a4ddcf7-d214-42b9-b27f-04d566d7706e',
    '5a4dde59-ed40-49ea-bbf4-04d566d7706e',
    '5a4dde59-3648-476d-8d96-04d566d7706e',
    '5a4ddff1-eb20-4bdb-bf81-04d466d7706e',
    '5a4ddff1-ee8c-4d83-b4dd-04d466d7706e',
    '5a4de025-d3e8-4266-9fd2-04d566d7706e',
    '5a4de025-7d7c-441c-acfa-04d566d7706e',
    '5a4de091-ebb8-4931-aebf-04d566d7706e',
    '5a4de091-09e4-4d32-b6d7-04d566d7706e',
    '5a4de467-dd68-4324-84e4-04d566d7706e',
    '5a57679d-ab38-452c-8596-356066d7706e',
    '5a57679d-efd8-4fba-a347-356066d7706e',
    '5a5489db-fc18-48e7-827c-321366d7706e',
    '5a6863a7-a1b8-4d8a-b562-04e166d7706e',
    '5a685293-ba6c-44f7-bedb-4f7066d7706e',
    '5a57281f-631c-43d2-ac89-051f66d7706e',
    '5a576fb2-3b8c-471e-88b3-052166d7706e',
    '5a685293-20d0-42c1-970d-4f7066d7706e',
    '5a685293-cce0-4d72-8196-4f7066d7706e',
    '5a685293-a764-46e3-8b90-4f7066d7706e',
    '5a6863a7-5bf4-4ee4-bc17-04e166d7706e',
    '5a685e83-c824-40e3-980b-04e166d7706e',
    '5a6889e5-3e98-44ec-9aad-04c166d7706e' ];


function testGet(conn, request) {



//console.log('request %s', request);
    pg.connect(conn, function (err, client, done) {
        if (err) {
            console.log('error fetching client from pool %s', err);
            console.log(err);
        }
        client.query(request, function (err, result) {
            done();
            if (err) {
                console.log(err);
            }
            console.log(new Date().getTime() - start);
            // console.log(result.rows);
            //// console.log(result.rows.map(r => r.id));
            // return;
            //saveToRedis(result.rows, uuid);
        });
    });

}


function testRedis(conn) {
    client.get(`${conn}:59f6f8f9-3944-4aa6-adb1-45a95cde535b`, function (error, result) {
        // console.log(result);
        console.log(new Date().getTime() - start);
    });
}

// //postgres test 200 recharches "simultanées"
//
// var conn = "postgres:idelibre:idelibre@localhost:5432/idelibre_libriciel"
//
// var request = squel.select()
//     .from("documents")
//     .field("path")
//     // .field("id")
//     //  .where("documents.id IN ?", tab)
//     .where("documents.id = ?", "59f6f8f9-3944-4aa6-adb1-45a95cde535b")
//     .toString();
//
// var start  = new Date().getTime();
// console.log("Start : " + start);
// for(var i =0 ; i <10000; i++){
//     testGet(conn, request);
// }




var start  = new Date().getTime();
console.log("Start : " + start);

for(var i =0 ; i <40000; i++)
{
    testRedis("5810a34f-c540-4ad1-9caa-189c5cde535b");
}


