var redis = require('redis');
var client = redis.createClient();
var fs = require("fs");
var logger = require("../logger");

function getFile(req, res){

    var id = '5a4ddff1-ee8c-4d83-b4dd-04d466d7706e'

    // client.get(id, sendDocument.call({res:res}));

    client.get(id, function(error, result){
        sendDocument(req, res, error, result)
    });

}


function sendDocument(req, res, error, filePath){
    console.log(error);
    if(!filePath || error) {
        res.send({sucess: false});
        return;
    }

    if (fs.existsSync(filePath)){
        // Yell if not reading permission on the directory
        fs.access(filePath, fs.constants.R_OK, function (err) {
            if (err) {
                logger.error("dlPdf error 500 error permissions for : ", {documentId: req.params["id"], file: filePath, suffix: req.token.suffix});
                res.statusCode = 500;
                res.write('500 sorry internal error');
                res.end();
            }else{
                logger.debug("dlPdf pdf send with success: ", {documentId: req.params["id"], suffix: req.token.suffix});
                console.log("Success");
                res.setHeader('Content-type', 'application/pdf');
                res.sendFile(filePath);
            }
        });

    } else {
        console.log("404 not found");
        logger.error("dlPdf error 404 not found : ", {documentId: req.params["id"], suffix: req.token.suffix});
        res.statusCode = 404;
        res.write('404 sorry not found');
        res.end();
    }


}



var req = {
    params :{
        id: "5a4ddff1-ee8c-4d83-b4dd-04d466d7706e"
    },
    token :{
        suffix : "libriciel"
    }
}

getFile(req, null);