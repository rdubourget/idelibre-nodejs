var app=require("../jasm");

describe("Addition",function(){
    it("The function should add 2 numbers",function() {
        var value=app.AddNumber(5,6);
        expect(value).toBe(101);
        expect(value).not.toBe(11);
    });
});