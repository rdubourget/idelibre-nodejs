//version 3.0.2 checkc

var app = require('express')();
var express = require('express');
var http = require('http').Server(app);
var io = require('socket.io')(http);


//connection postgresql
var pg = require('pg');
var squel = require("squel");
var fs = require("fs");
var morgan = require("morgan");


// pour recuperer req.body dans les requetes post
var bodyParser = require('body-parser');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

var sha1 = require('sha1');

var config = require("./config");
var logger = require('./logger');


logger.info("Start node service");
//JWT
var jwt = require('jsonwebtoken');
app.set('secret', config.secret);

app.use(morgan('dev'));
url = require('url');

/*
fs.writeFile('tmp/horodateFile2.txt',
    'seance id : 123456789\n\
convocation id : 315316135\n\
user id : 6549846561\n\
hash method : sha1\n\
hashfile : aeaz aze aze aze zae aze azez aze aze aze aze ar az zrzre zer zer zr zr\n\
filename : convocation.pdf',
    function (err) {

        if (err)
            throw err;

        console.log('It\'s saved! in root/tmp');

    });
*/






function verifyToken(req, res, next) {
    logger.debug('verifyToken');
    //acces allow all
    if (req.path === '/pushSeance' /* || modify or delete*/) {
        next();
        //check if logged
    } else {
        var token;
        //if it's a post request (never the wes here)
        if (req.method == 'POST') {
            token = req.body.token;
            if (!token) {
                logger.error('no token post');
                //send an error to the client
                res.sendStatus(403)
                return;
            }
            //if it's a get request
        } else {
            token = req.headers.token;
            if (!token) {
                logger.error('no token get');
                //send a error to the client
                res.sendStatus(403);
                return;
            }
        }

        jwt.verify(token, config.secret, function (err, decoded) {
            if (err) {
                //bad token
                logger.error('error reading token');
                res.sendStatus(403);
                return;
            } else {
                // if everything is good, save to request for use in other routes
                req.token = decoded;
                logger.debug('token ok');

                //then do the next action
                return next();
            }
        });
    }
}



///////////////////////////////////////////////////////////

//service static file (dl from filesystem)
//ex http://localhost/nodejs/pdf/verybig.pdf   hosted in /public/verybig.pdf
//app.use('/pdf', express.static(__dirname + '/public'));


//routing
var index010 = require('./0.1.0/routes/index');
var seances010 = require('./0.1.0/routes/seances');
var projets010 = require('./0.1.0/routes/projets');
var annexes010 = require('./0.1.0/routes/annexes');
var users010 = require('./0.1.0/routes/users');


//app.use('/0.1.0', verifyToken, index010);
// app.use('/nodejs/0.1.0/seances', verifyToken, seances010);
app.use('/nodejs/0.1.0/seances', verifyToken, seances010);
app.use('/nodejs/0.1.0/projets', verifyToken, projets010);
app.use('/nodejs/0.1.0/annexes', verifyToken, annexes010);
app.use('/nodejs/0.1.0/users', /*verifyToken,*/ users010);



var index020 = require('./0.2.0/routes/index');
var seances020 = require('./0.2.0/routes/seances');
var projets020 = require('./0.2.0/routes/projets');
var annexes020 = require('./0.2.0/routes/annexes');
var users020 = require('./0.2.0/routes/users');
var invitations020 = require('./0.2.0/routes/invitations');
// var documents020 = require('./0.2.0/routes/documents');
var zips020 = require('./0.2.0/routes/zips');


//app.use('/0.1.0', verifyToken, index010);
app.use('/nodejs/0.2.0/seances', verifyToken, seances010);
app.use('/nodejs/0.2.0/projets', verifyToken, projets010);
 // app.use('/nodejs/0.2.0/projets', verifyToken, documents020);
app.use('/nodejs/0.2.0/annexes', verifyToken, annexes010);
app.use('/nodejs/0.2.0/users', verifyToken, users010);
app.use('/nodejs/0.2.0/invitations', verifyToken, invitations020);
app.use('/nodejs/0.2.0/zips', verifyToken, zips020);
// app.use('/nodejs/0.2.0/documents', verifyToken, documents020);




var loadTest  = require('./0.1.0/routes/loadTest');
app.use('/0.1.0/loadTest', loadTest);
///////////////////
app.get('/pdftest', function (req, res) {
    var filePath = __dirname + '/public/' + 'Procurve2.pdf';
    if (fs.existsSync(filePath))
    {
        res.sendFile(filePath)
    }
    else
    {
        console.log('error');
        res.statusCode = 404;
        res.write('404 sorry not found');
        res.end();
    }


});


app.get('/boo/:mavar', function(req, res){
    res.send("toto");
});


app.get('/nodejs/checkConnection', function(req,res){
    console.log(config.version);
    res.send(config.version);
});


//websockets
var ws010 = require('./0.1.0/socketio');
var socket010 = ws010(io);


//websockets
var ws020 = require('./0.2.0/socketio');
var socket020 = ws020(io);



var Clients = require('./Clients');

// from php server
app.get('/pushSeance/:usersId/:auth', function (req, res) {
    logger.debug('/pushSeance');
    var socketId = Clients.getSocketByClient(req.params["usersId"]);
    if (req.params["auth"] == config.passPhrase) {
        if (socketId) {
            socket010.to(socketId).emit('newSeance', {message: "Hello !"});
            socket020.to(socketId).emit('newSeance', {message: "Hello !"});
        }
    }
    res.send('');
});

// from php server
app.get('/pushModifiedSeance/:usersId/:auth', function (req, res) {
    logger.debug('/pushmodified');
    var socketId = Clients.getSocketByClient(req.params["usersId"]);
    if (req.params["auth"] == config.passPhrase) {
        if (socketId) {
            socket010.to(socketId).emit('modifiedSeance', {message: "toto"});
            socket020.to(socketId).emit('modifiedSeance', {message: "toto"});

        }
    }
    res.send('');
});


app.get('/removeSeance/:usersId/:auth', function (req, res) {
    logger.debug('/removeSeance');
    var socketId = Clients.getSocketByClient(req.params["usersId"]);
    if (req.params["auth"] == config.passPhrase) {
        if(socketId) {
            socket010.to(socketId).emit('removeSeance', {message: "toto"});
            socket020.to(socketId).emit('removeSeance', {message: "toto"});
        }
    }
    res.send('');
});


/**
 * dynamically change log lovel;
 */
app.get('/nodejs/debugger/:level/:pass', function(req, res){
    logger.error("debugger : " + req.params.level);
    if(!req.params.pass == config.passPhrase){
        logger.error("wrong passphrase");
        return res.Send("error");
    }
    config.logLevel = req.params.level;
    logger.transports.console.level = config.logLevel;
    logger.transports.file.level = config.logLevel;
    res.send("ok");
});



// dynamically add collectivity suffix conn to the list
app.post('/nodejs/addCollectivite/', function (req, res) {
    var data = req.body;
    var key = Object.keys(data)[0];
    config.conMap[key] = data[key];
    res.send('ok');
});






http.listen(config.port, function () {
    logger.info('listening on *:' + config.port);
});
