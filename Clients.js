
var Clients ={};

Clients.clientsSocketsList = {};

Clients.getSocketByClient = function(clientId){
    return Clients.clientsSocketsList[clientId];
};

Clients.associateClientSocket = function( clientId, socketId){
    Clients.clientsSocketsList[clientId] = socketId;
};


//export config module
module.exports = Clients;