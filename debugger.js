//tail -f idelibreNode.log | grep -E  'montpellier-3m|idelibre_montpellier3m'

var http = require('http');
var config = require('./config');
var args = process.argv.slice(2);

if(!args[0]){
    console.log("no levels set");
    console.log("levels are : error, warn, info, verbose, debug, silly");
    return;
}

http.get("http://127.0.0.1:8080/debugger/" + args[0] +"/" + config.passPhrase, function(res){
    console.log("ok");
});
