
var bdd ={}

bdd.sqlReq = function (conn, request, res, traitement, req) {
    logger.debug("bddReqSocket : ", {conn: conn});
    logger.silly("bddReqSocket : ", {request: request});

    pg.connect(conn, function (err, client, done) {
        if (err) {
            logger.error("bddReq : connection error ", {conn: conn, error:err});
            res.statusCode = 500;
            res.write(err);
            res.end();
            return console.log('error fetching client from pool %s', err);
        }

        client.query(request, function (err, result) {
            //call `done()` to release the client back to the pool
            done();
            if (err) {
                logger.error("bddReq : request error ", {conn: conn, error:err});
                res.statusCode = 500;
                res.write(err);
                res.end();
                return console.log(err);

            }

            if (traitement) {
                traitement(conn, result.rows, res, req);
            } else {
                console.log(result.rows);
                var response = JSON.stringify(result.rows);
                logger.silly("bddReqSocket : send response ", {response: response, conn:conn});
                console.log("res");
                res.statusCode = 200;
                res.end();
            }
        });
    });
};


module.exports = bdd;




