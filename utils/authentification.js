var jwt = require('jsonwebtoken');
var logger = require('../logger');
var config = require("../config.js");

module.exports={


 verifyToken = function (action, data, res) {
        if (!data) {
            return;
        }
        if (!data.token) {
            //ask the client to loggin cause his token is empty
            nsp.to(socket.client.id).emit('tokenError', {success: false, message: 'no token'});
            return;
        }

        jwt.verify(data.token, config.secret, function (err, decoded) {
            if (err) {
                //bad token
                nsp.to(socket.client.id).emit('tokenError', {success: false, message: 'invalid token'});
            } else {
                // if everything is good, save to request for use in other routes
                data.token = decoded;
                //then do the next action
                action(data, socket);
            }
        });
    };

}