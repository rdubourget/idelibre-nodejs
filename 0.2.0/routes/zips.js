var express = require('express');
var router = express.Router();
var pg = require('pg');
var squel = require("squel");
var logger = require("../../logger");
var config = require("../../config");
var fs = require("fs");




router.get('/dlZip/:seanceId', function (req, res) {
    //get the collId

    console.log({seanceId: req.params["seanceId"], suffix: req.token.suffix});
    logger.debug('dlZip', {seanceId: req.params["seanceId"], suffix: req.token.suffix});

    var request = squel.select()
        .from("collectivites")
        .field("id")
        .where("login_suffix = ?", req.token.suffix)
        .toString();

    var conn = config.MASTER_DATABASE;


    pg.connect(conn, function (err, client, done) {
        if (err) {
            console.log('error fetching client from pool %s', err);
            console.log(err);
            logger.error('dlZIP : database connection error');
            res.statusCode = 500;
            res.write('500 internal error');
            return res.end();
        }
        client.query(request, function (err, result) {
            //call `done()` to release the client back to the pool
            done();
            if (err) {
                logger.error(err);
                res.statusCode = 500;
                logger.error('dlZIP : request error');
                res.write('500 internal error');
                return res.end();
            }

            var id = result.rows[0].id;

            console.log("res id : " + id );
            var filePath = `${config.zipPath}${id}/${req.params["seanceId"]}.zip`;


            if (fs.existsSync(filePath))
            {
                // Yell if not reading permission on the directory
                fs.access(filePath, fs.constants.R_OK, function (err) {
                    if (err) {
                        logger.error("dlZIP error 500 error permissions for : ", {documentId: req.params["seanceId"], file: filePath, suffix: req.token.suffix});
                    } else {
                        logger.debug("dlZIP annexe send with success: ", {documentId: req.params["seanceId"], suffix: req.token.suffix});
                    }
                });

                res.header('Content-type', 'application/zip');
                res.sendFile(filePath);


            } else
            {
                logger.error('dlZip : 404 not found', {suffix: req.token.suffix});
                res.statusCode = 404;
                res.write('404 sorry not found');
                res.end();
            }
        });
    });

});







module.exports = router;





