var redis = require('redis');
var client = redis.createClient();
var fs = require("fs");
var logger = require("../../logger");
var express = require('express');
var router = express.Router();
var config = require("../../config");


router.get('/dlPdf/:id', function (req, res) {
    console.log("redisPDF");
    var id = req.params["id"];
    var suffix = req.token.suffix;
    var collUuid = config.suffixUuidMap[suffix];
    console.log(`${collUuid}:${id}`);
    client.get(`${collUuid}:${id}`, function(error, result){
        sendDocument(req, res, error, result)
    });
});


function sendDocument(req, res, error, filePath){
    console.log("senddoc");
    console.log(filePath);
    if(!filePath || error) {
        res.write('500 sorry internal error');
        res.end();
        return;
    }
console.log(filePath);
    if (fs.existsSync(filePath)){
        console.log("filePath");
        // Yell if not reading permission on the directory
        fs.access(filePath, fs.constants.R_OK, function (err) {
            if (err) {
                logger.error("dlPdf error 500 error permissions for : ", {documentId: req.params["id"], file: filePath, suffix: req.token.suffix});
                res.statusCode = 500;
                res.write('500 sorry internal error');
                res.end();
            }else{
                logger.debug("dlPdf pdf send with success: ", {documentId: req.params["id"], suffix: req.token.suffix});
                console.log("Success");
                res.setHeader('Content-type', 'application/pdf');
                res.sendFile(filePath);
            }
        });

    } else {
        console.log("404 not found");
        logger.error("dlPdf error 404 not found : ", {documentId: req.params["id"], suffix: req.token.suffix});
        res.statusCode = 404;
        res.write('404 sorry not found');
        res.end();
    }
}


module.exports = router;