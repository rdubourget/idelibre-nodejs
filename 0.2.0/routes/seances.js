var express = require('express');
var router = express.Router();
var squel = require("squel");
var logger = require("../../logger");

/* GET home page. */
router.get('/', function(req, res) {
  res.send("bienvenue à l'acceuil");
});





router.get('/getAll', function (req, res) {
    logger.debug("seance getAll");
    
    //recuperation de toutes les seances a
    var request = squel.select()
            .from("convocations")
            .join("seances", null, "convocations.seance_id = seances.id")
            .join("projets", null, "projets.seance_id = seances.id")
            .join("pthemes", null, "projets.ptheme_id = pthemes.id")
            .left_join("annexes", null, "projets.id = annexes.projet_id")


            .field('seances.id', "seance_id")
            .field("seances.name", 'seance_name')
            .field("seances.rev", 'seance_rev')
            .field("seances.date_seance", "seance_date")
            .field("seances.place", "seance_place")
            .field("seances.document_id", "seance_document_id")

            .field("projets.name", 'projet_name')
            .field("projets.id", "projet_id")
            .field("projets.rank", "projet_rank")
            .field("projets.document_id", "projet_document_id")
            .field("pthemes.name", 'ptheme_name')
            .field("convocations.read", "convocation_read")

            .field("annexes.id", "annexe_id")
            .field("annexes.name", "annexe_name")

            .where("convocations.user_id = ?", '55ed8758-cf7c-4893-b30c-3bb066d7706e')

            .where("convocations.active = true")
            .toString();

//TODO order by seances ID !!!

    console.log(request);
    // var request = "select content from documents where id = '" + req.params["id"] + "';";
  //  var conn = "postgres://idelibre:idelibre@localhost:5433/idelibre_newcollfr";
  //  bddReqSocket(conn, request, null, null, formatgetAll);

});


module.exports = router;