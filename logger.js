
var winston = require('winston');
var config = require('./config');




logger = new winston.Logger({
    transports: [
        new winston.transports.Console({
            level: config.logLevel,
            handleExceptions: true,
            json: false,
            colorize: true
        }),
        new winston.transports.File({
            filename: config.logPath,
            level: config.logLevel,
            handleExceptions: true,
            json: false,
            colorize: true
        })

    ],
    exitOnError: false
}); 

//export du module de config
module.exports = logger;
