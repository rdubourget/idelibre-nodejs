//  root path = ../../

var express = require('express');
var router = express.Router();
var pg = require('pg');
var squel = require("squel");
var logger = require("../../logger");
var config = require("../../config");
var fs = require("fs");


router.get('/', function (req, res) {
    res.send("annexes");
});


var getConnectionBySuffix = function (suffix) {
    return config.conMap[suffix];
};


router.get('/dlAnnexe/:id', function (req, res) {
    logger.debug('dlAnnexe', {annexeid: req.params["id"], suffix: req.token.suffix});
    var request = squel.select()
            .from("annexes")
            .field("path")
            .field("type")
            .where("id = ?", req.params["id"])
            .toString();

    console.log(request);

    var conn = getConnectionBySuffix(req.token.suffix);
    if (!conn) {
        logger.error('dlAnnexe : no connection for', {suffix: req.token.suffix});
    }

    //console.log('request %s', request);
    pg.connect(conn, function (err, client, done) {
        if (err) {
            console.log('error fetching client from pool %s', err);
            console.log(err);
            logger.error('dlAnnexe : database connection error', {suffix: req.token.suffix});
            res.statusCode = 500;
            res.write('500 internal error');
            return res.end();
        }
        client.query(request, function (err, result) {
            //call `done()` to release the client back to the pool
            done();
            if (err) {
                logger.error(err);
                res.statusCode = 500;
                logger.error('dlAnnexe : request error', {suffix: req.token.suffix, annexeId: req.params["id"]});
                res.write('500 internal error');
                return res.end();
            }

            var filePath = result.rows[0].path;

            if (fs.existsSync(filePath))
            {
                // Yell if not reading permission on the directory
                fs.access(filePath, fs.constants.R_OK, function (err) {
                    if (err) {
                        logger.error("dlAnnexe error 500 error permissions for : ", {documentId: req.params["id"], file: filePath, suffix: req.token.suffix});
                    } else {
                        logger.debug("dlAnnexe annexe send with success: ", {documentId: req.params["id"], suffix: req.token.suffix});
                    }
                });

                res.header('Content-type', result.rows[0].type);
                res.sendFile(filePath);


            } else
            {
                logger.error('dlAnnexe : 404 not found', {suffix: req.token.suffix});
                res.statusCode = 404;
                res.write('404 sorry not found');
                res.end();
            }
        });
    });
});


module.exports = router;

