var express = require('express');
var router = express.Router();
var pg = require('pg');
var squel = require("squel");
var logger = require("../../logger");
var config = require("../../config");



router.get('/', function (req, res) {
    updateSeances(null, res)
});
  
  
  function updateSeances(data, res) {
        console.log("updateSeances");


        //ask all seances
        var request = squel.select()
                .from("convocations")
                .join("seances", null, "convocations.seance_id = seances.id")
                .join("projets", null, "projets.seance_id = seances.id")
                .join("pthemes", null, "projets.ptheme_id = pthemes.id")
                .left_join("annexes", null, "projets.id = annexes.projet_id")

                .field('seances.id', "seance_id")
                .field("seances.name", 'seance_name')
                .field("seances.rev", 'seance_rev')
                .field("seances.date_seance", "seance_date")
                .field("seances.place", "seance_place")
                .field("seances.document_id", "seance_document_id")

                .field("projets.name", 'projet_name')
                .field("projets.id", "projet_id")
                .field("projets.rank", "projet_rank")
                .field("projets.document_id", "projet_document_id")
                .field("pthemes.name", 'ptheme_name')
                .field("convocations.read", "convocation_read")

                .field("annexes.id", "annexe_id")
                .field("annexes.name", "annexe_name")

                .where("convocations.user_id = ?", '55ed8758-cf7c-4893-b30c-3bb066d7706e')

                .where("convocations.active = true")
                .order("seance_id")
                .toString();

        var conn = "postgres://idelibre:idelibre@localhost:5433/idelibre_newcollfr";
        bddReq(conn, request, res);
    }
    
    
    
    bddReq = function(conn, request, res){

    //console.log('request %s', request);
    pg.connect(conn, function (err, client, done) {
        if (err) {
            return console.log('error fetching client from pool %s', err);
        }
        client.query(request, function (err, result) {
            //call `done()` to release the client back to the pool
            done();
            if (err) {
                return console.log(err);
            }
            
            res.send(result.rows);
        });
    });
    }
    
    
    
    
    module.exports = router;