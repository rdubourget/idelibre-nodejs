var express = require('express');
var router = express.Router();
var pg = require('pg');
var squel = require("squel");
var logger = require("../../logger");
var config = require("../../config");
var fs = require("fs");


/* GET home page. */
router.get('/', function (req, res) {
    res.send("Projet");
});



var getConnectionBySuffix = function (suffix) {
    return config.conMap[suffix];
};

//useless only for test
router.get('/pdftest', function (req, res) {
    logger.debug('pdftest');
    var filePath = __dirname + '/public/' + 'verybig.pdf';

    console.log(filePath);
    if (fs.existsSync(filePath))
    {
        logger.debug('sendfile');
        res.sendfile(filePath);
    } else
    {
        logger.error('errorwhile sending file');
        res.statusCode = 404;
        res.write('404 sorry not found');
        res.end();
    }


});




//dl from file system
//router.get('/dlfs/:id', function (req, res) {
router.get('/dlPdf/:id', function (req, res) {
    logger.debug("dlPdf", {documentId: req.params["id"], suffix: req.token.suffix});

    var request = squel.select()
            .from("documents")
            .field("path")
            .where("documents.id = ?", req.params["id"])
            .toString();

    var conn = getConnectionBySuffix(req.token.suffix);
    
    if(!conn){
        logger.error("no database connection for : ", {suffix: req.token.suffix});
    }

    //console.log('request %s', request);
    pg.connect(conn, function (err, client, done) {
        if (err) {
            console.log('error fetching client from pool %s', err);
            console.log(err);
            logger.error("dlPdf error 500 can't connect to database : ", {documentId: req.params["id"], suffix: req.token.suffix});
            res.statusCode = 500;
            res.write('500 sorry not found');
            return res.end();
        }
        client.query(request, function (err, result) {
            //call `done()` to release the client back to the pool
            done();
            if (err) {
                console.log(err);
                logger.error("dlPdf error 500 bad request : ", {documentId: req.params["id"], suffix: req.token.suffix});
                res.statusCode = 500;
                res.write('500 sorry not found');
                return res.end();
            }

            var filePath = result.rows[0].path;

            if (fs.existsSync(filePath))
            {
                // Yell if not reading permission on the directory
                fs.access(filePath, fs.constants.R_OK, function (err) {
                    if (err) {
                      logger.error("dlPdf error 500 error permissions for : ", {documentId: req.params["id"], file: filePath, suffix: req.token.suffix});
                    }else{
                        logger.debug("dlPdf pdf send with success: ", {documentId: req.params["id"], suffix: req.token.suffix});
                    }
                });

                res.setHeader('Content-type', 'application/pdf');
                res.sendFile(filePath);

            } else
            {
                console.log("404 not found");
                logger.error("dlPdf error 404 not found : ", {documentId: req.params["id"], suffix: req.token.suffix});
                res.statusCode = 404;
                res.write('404 sorry not found');
                res.end();
            }


        });
    });



});

module.exports = router;
