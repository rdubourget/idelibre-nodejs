var express = require('express');
var router = express.Router();
var squel = require("squel");
var logger = require("../../logger");
var fs = require("fs");

/* GET home page. */
router.get('/', function(req, res) {
    res.send("bienvenue à l'acceuil");
});


router.get('/getzipSeance/:id', function (req, res) {

    logger.debug("dlPdf", {documentId: req.params["id"], suffix: req.token.suffix});

    var request = squel.select()
        .from("documents")
        .field("path")
        .where("documents.id = ?", req.params["id"])
        .toString();

    var conn = getConnectionBySuffix(req.token.suffix);

    if (!conn) {
        logger.error("no database connection for : ", {suffix: req.token.suffix});
    }

    //console.log('request %s', request);
    pg.connect(conn, function (err, client, done) {
        if (err) {
            console.log('error fetching client from pool %s', err);
            console.log(err);
            logger.error("dlPdf error 500 can't connect to database : ", {
                documentId: req.params["id"],
                suffix: req.token.suffix
            });
            res.statusCode = 500;
            res.write('500 sorry not found');
            return res.end();
        }
        client.query(request, function (err, result) {
            //call `done()` to release the client back to the pool
            done();
            if (err) {
                console.log(err);
                logger.error("dlPdf error 500 bad request : ", {
                    documentId: req.params["id"],
                    suffix: req.token.suffix
                });
                res.statusCode = 500;
                res.write('500 sorry not found');
                return res.end();
            }

            var filePath = result.rows[0].path;

            if (fs.existsSync(filePath)) {
                // Yell if not reading permission on the directory
                fs.access(filePath, fs.constants.R_OK, function (err) {
                    if (err) {
                        logger.error("dlPdf error 500 error permissions for : ", {
                            documentId: req.params["id"],
                            file: filePath,
                            suffix: req.token.suffix
                        });
                    } else {
                        logger.debug("dlPdf pdf send with success: ", {
                            documentId: req.params["id"],
                            suffix: req.token.suffix
                        });
                    }
                });

                res.setHeader('Content-type', 'application/pdf');
                res.sendFile(filePath);

            } else {
                console.log("404 not found");
                logger.error("dlPdf error 404 not found : ", {documentId: req.params["id"], suffix: req.token.suffix});
                res.statusCode = 404;
                res.write('404 sorry not found');
                res.end();
            }

        });


    });
});



router.get('/getAll', function (req, res) {
    logger.debug("seance getAll");

    //recuperation de toutes les seances a
    var request = squel.select()
        .from("convocations")
        .join("seances", null, "convocations.seance_id = seances.id")
        .join("projets", null, "projets.seance_id = seances.id")
        .join("pthemes", null, "projets.ptheme_id = pthemes.id")
        .left_join("annexes", null, "projets.id = annexes.projet_id")


        .field('seances.id', "seance_id")
        .field("seances.name", 'seance_name')
        .field("seances.rev", 'seance_rev')
        .field("seances.date_seance", "seance_date")
        .field("seances.place", "seance_place")
        .field("seances.document_id", "seance_document_id")

        .field("projets.name", 'projet_name')
        .field("projets.id", "projet_id")
        .field("projets.rank", "projet_rank")
        .field("projets.document_id", "projet_document_id")
        .field("pthemes.name", 'ptheme_name')
        .field("convocations.read", "convocation_read")

        .field("annexes.id", "annexe_id")
        .field("annexes.name", "annexe_name")

        .where("convocations.user_id = ?", '55ed8758-cf7c-4893-b30c-3bb066d7706e')

        .where("convocations.active = true")
        .toString();

//TODO order by seances ID !!!

    console.log(request);
    // var request = "select content from documents where id = '" + req.params["id"] + "';";
    //  var conn = "postgres://idelibre:idelibre@localhost:5433/idelibre_newcollfr";
    //  bddReqSocket(conn, request, null, null, formatgetAll);

});


module.exports = router;