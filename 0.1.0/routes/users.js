// Useless Class now


var express = require('express');
var router = express.Router();
var squel = require('squel');
var pg = require("pg");
var sha1 = require("sha1");
var jwt = require('jsonwebtoken');
var bdd = require('../../utils/bdd')

var config = require("../../config.js");
var logger = require("../../logger");
/* GET home page. */



// var request = `
//     select  json_build_object(
//         'typid', types.id,
//         'name', types.name,
//         'seances', (select json_agg(json_build_object('seanceid', seances.id, 'seancename', seances.name))
//             from seances where types.id = seances.type_id
//         )
//     ) as types
//     from types`;



/**
 * Useless
 */
router.get('/seances', function (req, res) {



    var request = `
    select  json_build_object(
        'typid', types.id,
        'name', types.name,
        'seances', (select json_agg(json_build_object('seanceid', seances.id, 'seancename', seances.name))
            from seances where types.id = seances.type_id
        )
    ) as types
    from types`;


    // var request = `select (json_agg(json_build_object('seanceid', seances.id, 'seancename', seances.name))) from seances`;

    var request = `
    select  json_build_object(
        'convocation_read', convocations.read,
        'convocation_id' ,convocations.id,
        'seance', (select json_build_object(
            'seance_id', seances.id, 
            'seance_name', seances.name,
            'seance_rev', seances.rev,
            'seance_date', seances.date_seance,
             'seance_place', 'seances.place',
             'seance_document_id', seances.document_id,            
            'projets', (select json_agg(json_build_object(
                'projet_id', projets.id,
                'projet_name', 'projets.name',
                'projet_rank', 'projets.rank',
                'projet_document_id', projets.document_id,
                'projet_user_id', projets.user_id,
                'user', (select json_build_object(
                    'projet_user_lastname', users.lastname, 
                    'projet_user_firstname', users.firstname 
                )
                from users where users.id = projets.user_id
                ),
                
                'annexes', (select json_agg(json_build_object(
                    'annexe_id', 'annexes.id'
                ))
                from annexes where annexes.projet_id = projets.id
                ),
                'ptheme', (select json_build_object(
                    'ptheme_name', pthemes.fullname
                )
                from pthemes where pthemes.id = projets.ptheme_id
                )
            ))
            from projets where projets.seance_id = seances.id
            )
        )
        from seances where convocations.seance_id = seances.id)
    ) as convocation
    from convocations`;


    var conn = "postgres://idelibre:idelibre@localhost:5432/idelibre_libriciel";
    bddReq(conn, request, res);

});
//  // 'seances', (select  json_build_object('id', seances.id, 'name', seances.name ) from seances where types.id = seances.type_id )
/**
 * currentPassword, newPassword
 */
router.post('/changePassword', function(req,res){
    	var conn = config.conMap[req.token.suffix];
	console.log("change Password");
	console.log(req.body);
	
	if(!conn){
        logger.error("no database connection for : ", {suffix: req.token.suffix});
        res.write('500 sorry no database found');
        res.statusCode = 500;
        res.end();
        return;
    }

    if(!req.body.currentPassword || !req.body.newPassword){
        res.statusCode = 500;
	//res.send({success: false, message:"wrong password"});
	res.end();           
        return;
    }


    var passwordSalt = config.salt + req.body.currentPassword;
    var passwordSha1 = sha1(passwordSalt);

    var request = squel.select()
        .from("users")
        .field("users.id", "user_id")
        .field("users.password", "user_password")
        .where("users.id = ?", req.token.user_id)
        .where("users.password = ?", passwordSha1)
        .toString();

    bddReq(conn, request, res, setNewPassword, req)
    //  bdd.sqlReq(conn, request, res, setNewPassword, req)
});



var bddReq = function (conn, request, res, traitement, req) {
    logger.debug("bddReqSocket : ", {conn: conn});
    logger.silly("bddReqSocket : ", {request: request});

    pg.connect(conn, function (err, client, done) {
        if (err) {
            logger.error("bddReq : connection error ", {conn: conn, error:err});
            res.statusCode = 500;
            res.write(err);
            res.end();
            return console.log('error fetching client from pool %s', err);
        }

        client.query(request, function (err, result) {
            //call `done()` to release the client back to the pool
            done();
            if (err) {
                logger.error("bddReq : request error ", {conn: conn, error:err});
                res.statusCode = 500;
                res.write(err);
                res.end();
                return console.log(err);

            }


            if (traitement) {
                traitement(conn, result.rows, res, req);
            } else {
                console.log(result.rows);
                var response = JSON.stringify(result.rows);
                logger.silly("bddReqSocket : send response ", {response: response, conn:conn});
                console.log("res");
                // res.statusCode = 200;
                // res.write("success");
                // res.end();
                // res.send(response);
                res.send({success: true});
            }
        });
    });
};


var setNewPassword = function(conn, result, res, req){

    console.log("setNewPassword");
    if (result && result.length > 0){


        if(!req.body.newPassword){
            res.statusCode = 500;
            res.end();
            return;
        }
        var passwordSalt = config.salt + req.body.newPassword;
        var passwordSha1 = sha1(passwordSalt);

        var request = squel.update()
            .table("users")
            .set("password", passwordSha1)
            .where("id = ?", req.token.user_id )
            .toString()


        bddReq(conn, request, res, null, req)

    }else{
        res.statusCode = 403;
        res.end();
        return;
    }
}


/**
 * useless
 */
router.post('/authenticate', function (req, res) {

    var conn = "postgres://idelibre:idelibre@localhost:5433/idelibre_newcollfr";
    var request = squel.select()
        .from("users")
        .field("users.id", "user_id")
        .field("users.username", "user_username")
        .field("users.group_id", "group_id")
        .field("users.password", "user_password")
        .where("users.id = ?", '55ed8758-cf7c-4893-b30c-3bb066d7706e') //TODO : data.userId
        .toString();

    pg.connect(conn, function (err, client, done) {
        if (err) {
            return console.log('error fetching client from pool %s', err);
        }
        client.query(request, function (err, result) {
            //call `done()` to release the client back to the pool
            done();
            if (err) {
                return console.log(err);
            }
            //res.setHeader('Content-type', 'application/pdf');
            res.send(result.rows);
            console.log(result.rows);

            var user = result.rows[0];
            //if user doesn't exist send error 
            if (!user) {
                res.json({success: false, message: 'Authentication failed. User not found.'});
            } else {
                // if user exist
                //dataJson = json.parse(req.body)
                var passwordSalt = config.salt + dataJson.password;
                var passwordSha1 = sha1(passwordSalt);
                if (user.password != passwordSha1) {
                    res.json({success: false, message: 'Authentication failed. Wrong password.'});
                } else {
                    var token = jwt.sign(user, config.secret, {
                        expiresInMinutes: 1440
                    });
                    res.json({success: true, message: 'Enjoy your token!', token: token});
                }
            }
        });
    });
});


/**
 * useless
 */
router.get('/authenticate', function (req, res) {

    var conn = "postgres://idelibre:idelibre@localhost:5433/idelibre_newcollfr";
    var request = squel.select()
        .from("users")
        .field("users.id", "user_id")
        .field("users.username", "user_username")
        .field("users.group_id", "group_id")
        .field("users.password", "password")
        .where("users.id = ?", '55ed8758-cf7c-4893-b30c-3bb066d7706e') //TODO : data.userId
        .toString();

    pg.connect(conn, function (err, client, done) {
        if (err) {
            return console.log('error fetching client from pool %s', err);
        }
        client.query(request, function (err, result) {
            //call `done()` to release the client back to the pool
            done();
            if (err) {
                return console.log(err);
            }
            //res.setHeader('Content-type', 'application/pdf');
            //  res.send(result.rows);
            console.log(result.rows);

            var user = result.rows[0];
            //if user doesn't exist send error 
            if (!user) {
                res.json({success: false, message: 'Authentication failed. User not found.'});
            } else {
                // if user exist
                //dataJson = json.parse(req.body)
                var passwordSalt = config.salt + "idelibre";
                var passwordSha1 = sha1(passwordSalt);
                console.log(user.password);
                console.log(passwordSha1);
                console.log(config.salt);
                if (user.password != passwordSha1) {
                    res.json({success: false, message: 'Authentication failed. Wrong password.'});
                } else {
                    console.log(user, config.secret);
                    var token = jwt.sign(user, config.secret, {
                        expiresIn: 86400
                    });
                    res.json({success: true, message: 'tocken', token: token});
                }
            }
        });
    });
});






module.exports = router;
