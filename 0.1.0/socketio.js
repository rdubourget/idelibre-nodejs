/**
 *  socket io listener api 0.1.0
 *test
 */
//  root path = ../

var squel = require("squel");
var pg = require("pg");
var sha1 = require("sha1");

var config = require("../config.js");
var logger = require('../logger');
var _ = require('underscore');
var jwt = require('jsonwebtoken');
var Clients = require("../Clients.js");



module.exports = function (io) {


    var nsp = io.of("/0.1.0");


    //remove autoquote for alias table name
    squel.cls.DefaultQueryBuilderOptions.autoQuoteAliasNames = false;
    squel.cls.DefaultQueryBuilderOptions.replaceSingleQuotes = true;


    /**
     * return connection to database
     * @param suffix
     * @returns {*}
     */
    var getConnectionBySuffix = function (suffix) {
        return config.conMap[suffix];
    };



    /**
     * check token and continue if call action(data, socket) if token ok
     * @param {type} action
     * @param {type} data (username, password, suffix)
     * @param {type} socket
     * @returns {undefined}
     */
    verifyToken = function (action, data, socket) {
        logger.debug("verifyToken : " + data);

        if (!data) {
            logger.error('verifyToken : no data');
            return;
        }
        if (!data.token) {
            logger.error('verifyToken : no token', {suffix: data.suffix});
            //ask the client to loggin cause his token is empty
            nsp.to(socket.client.id).emit('tokenError', JSON.stringify({success: false, message: 'no token'}));
            return;
        }

        jwt.verify(data.token, config.secret, function (err, decoded) {
            if (err) {
                //bad token
                logger.error('verify token :  token error', {suffix: data.suffix});
                nsp.to(socket.client.id).emit('tokenError', JSON.stringify({success: false, message: 'invalid token'}));
            } else {
                logger.silly("verifyToken : token ok" + JSON.stringify(decoded));
                // if everything is good, save to request for use in other routes
                data.token = decoded;
                //then do the next action
                action(data, socket);
            }
        });
    };


    //new connection
    nsp.on('connection', function (socket) {
        logger.debug('on connection test');
        logger.debug('on connection2');

        //send message to confirm the connection to the client
        nsp.to(socket.client.id).emit('hello');

        socket.on('disconnect', function () {
            logger.debug('disconnect');
        });


        //push from php server
        socket.on('seance push', function () {
            logger.debug('seancepush');
            socket.broadcast.emit('newSeance', null);
        });



        /*        socket.on('todo', function (data) {
         logger.debug('todo');
         verifyToken(todo, data, socket);
         });
         */

        //AR from convocation read
        socket.on('ARs', function (data) {
            logger.debug("ARS : ", {suffix: data.suffix});
            verifyToken(ars, data, socket);
        });

        socket.on('updateSeances', function (data) {
            logger.debug('updateSeances', {suffix: data.suffix});
            verifyToken(updateSeances, data, socket);
        });


        socket.on('archivedSeancesList', function (data) {
            logger.debug('archivedSeancesList', {suffix: data.suffix});
            verifyToken(archivedSeancesList, data, socket);
        });


        socket.on('archivedProjetsList', function (data) {
            logger.debug('archivedProjetsList', {suffix: data.suffix});
            verifyToken(archivedProjetsList, data, socket);
        });


        socket.on('getUserList', function (data) {
            logger.debug('getUserList', {suffix: data.suffix});
            verifyToken(getUserList, data, socket);
        });

        //when send annotations
        socket.on("annotation", function (data) {
            logger.debug("annotation", {suffix: data.suffix});
            verifyToken(addToqueue, data, socket);

        });


        socket.on("readAnnotationsConnect", function (data) {
            logger.debug("readAnnotationsConnect", {suffix: data.suffix});
            verifyToken(readAnnotationsConnect, data, socket);
        });


        socket.on("deleteAnnotations", function (data) {
            logger.debug('deleteAnnotations', {suffix: data.suffix})
            verifyToken(deleteAnnotations, data, socket);

        });


        socket.on("getAnnotations", function (data) {
            logger.debug('getAnnotations', {suffix: data.suffix})
            verifyToken(getAnnotations, data, socket);

        });


        socket.on("sendAnnotationRead", function (data) {
            logger.debug('sendAnnotationRead', {suffix: data.suffix})
            verifyToken(sendAnnotationRead, data, socket);

        });


        socket.on("deleteAnnotationsConnect", function (data) {
            logger.debug('deleteAnnotationsConnect', {suffix: data.suffix})
            verifyToken(deleteAnnotationsConnect, data, socket);

        });

        socket.on("sendPresence", function(data){
            verifyToken(confirmPresence, data, socket);
        });


        //needs data.username and data.password
        socket.on('authenticate', function (data) {
            logger.debug('authenticate : ', {suffix: data.suffix});
            logger.silly('authenticate : ', data);
            if (!data || !data.username || !data.password) {
                logger.error("authenticate : missing data, username or password");
                nsp.to(socket.client.id).emit('authFeedback', JSON.stringify({success: false, message: 'bad values'}));
                return;
            }
            var conn = getConnectionBySuffix(data.suffix);

            if (!conn) {
                logger.error("authentificate : no database connection for Arnaud :", {suffix: data.suffix});
                return nsp.to(socket.client.id).emit('authFeedback', {success: false, message: 'undeclaredConnection.', token: null, userId: null});
            }

            var request = squel.select()
                    .from("users")
                    .field("users.id", "user_id")
                    .field("users.username", "user_username")
                    .field("users.group_id", "group_id")
                    .field("users.password", "password")
                    .where("users.username = ?", data.username)
                    .toString();


            pg.connect(conn, function (err, client, done) {
                if (err) {
                    nsp.to(socket.client.id).emit('authFeedback', {success: false, message: 'suffix.error', token: null, userId: null});
                    logger.error("authenticate : error 500 can't connect to database : ", {suffix: data.suffix});
                    return console.log('error fetching client from pool %s', err);
                }
                client.query(request, function (err, result) {
                    //call `done()` to release the client back to the pool
                    done();
                    if (err) {
                        logger.error("authenticate : error 500 request error : ", {suffix: data.suffix});
                        nsp.to(socket.client.id).emit('authFeedback', {success: false, message: 'Authentication failed. database error.', token: null, userId: null});
                        return console.log(err);

                    }

                    //hack ; remove the anonmous objet type
                    var userAnonimous = result.rows[0];
                    var user = Object.assign({}, userAnonimous)

                    console.log(user);

                    //if user doesn't exist send error 
                    if (!user) {
                        logger.debug('authenticate : invalid username', {username: data.username, suffix: data.suffix});
                        nsp.to(socket.client.id).emit('authFeedback', {success: false, message: 'Authentication failed. User not found.', token: null, userId: null});
                        return;
                        //res.json({success: false, message: 'Authentication failed. User not found.'});
                    } else {
                        // if user exist
                        var passwordSalt = config.salt + data.password;
                        var passwordSha1 = sha1(passwordSalt);

                        if (user.password != passwordSha1) {
                            logger.debug('authenticate : invalid password', {username: data.username, suffix: data.suffix});
                            nsp.to(socket.client.id).emit('authFeedback', {success: false, message: 'Authentication failed. Wrong password.', token: null, userId: null});
                            return;

                        } else {
                            //add the suffix parameter;
                            user.suffix = data.suffix;
                            var token = jwt.sign(user, config.secret, {
                                expiresIn: config.tokenTimeout
                            });
                            logger.debug('authenticate : send a token to user', {username: data.username, suffix: data.suffix});
                            nsp.to(socket.client.id).emit('authFeedback', {success: true, message: 'token', token: token, userId: user.user_id});
                            //associate client to socketId
                            Clients.associateClientSocket(user.user_id, socket.client.id);
                        }
                    }
                });
            });
        });
    });




    /**
     *
     * @param {type} data  data.seanceId
     * @param {type} socket
     * @returns {undefined}
     */
    var getUserList = function (data, socket) {
        logger.debug('getUserList', {seanceId: data.seanceId, suffix: data.token.suffix});
        var request = squel.select()
                .from("convocations")
                .left_join("users", null, "convocations.user_id = users.id")
                .left_join("groupepolitiques", null, "users.groupepolitique_id = groupepolitiques.id")

                .field("users.id", "id")
                .field("users.firstname", "firstname")
                .field("users.lastname", "lastname")
                .field("users.username", "username")

                .field("groupepolitiques.id", "groupepolitique_id")
                .field("groupepolitiques.name", "groupepolitique_name")

                .field("convocations.seance_id", "seance_id")

                .where("convocations.seance_id = ?", data.seanceId)
                .toString();

        var conn = getConnectionBySuffix(data.token.suffix);

        bddReqSocket(conn, request, socket.client.id, 'userList', null, null);
    };


    // array of {token, annotation}
    var annotationQueue = [];
    var isRunningAnnotationQueue = false;

    var addToqueue = function (data, socket) {
        logger.debug('addtoQueue', {seanceId: data.annotation, suffix: data.token.suffix});
        try {
            for (var i = 0, l = data.annotation.length; i < l; i++) {
                annotationQueue.push({token: data.token, annotation: JSON.parse(data.annotation[i])});
            }
        } catch (e) {
            console.log(e);
        }
        manageQueue();

    };


    var manageQueue = function () {
        logger.debug("managequeue");
        if (isRunningAnnotationQueue === false && annotationQueue.length > 0) {
            logger.debug("queue is free")
            isRunningAnnotationQueue = true;
            //get the first element
            var data = annotationQueue[0];
            //remove the first element
            annotationQueue.splice(0, 1);

            if (data) {
                runQueue(data);
            } else {
                isRunningAnnotationQueue = false;
                manageQueue();
            }
        }else{
            logger.silly("queue already running or is empty" + annotationQueue.length);
        }
    };


    var runQueue = function (data) {
        logger.debug("runQueue");

        //check if annotation exists
        var request = squel.select()
                .from("annotationv3")
                .where("id = ?", data.annotation.annotation_id)
                .toString();

        var conn = getConnectionBySuffix(data.token.suffix);

        bddReqSocket(conn, request, null, '', annotationAction, data);

    };

    /**
     * Callback from the test of existence of annotation
     * @param {type} result
     * @param {type} socketClientId
     * @param {type} title
     * @param {type} data
     * @returns {undefined}
     */
    var annotationAction = function (result, socketClientId, title, data) {
        logger.debug("annotationAction");
        //if annotation already exist delete it before recreate it
        if (result) {
            deleteAnnotation(data);
        }
        //new annotation
        else {
            insertAnnotation([], '', "addNewAnnotationFeedback", data);
        }
    }


    /**
     * delete before update not real delete annotation
     * @param {type} data
     * @returns {undefined}
     */
    var deleteAnnotation = function (data) {
        var request = squel.delete()
                .from("annotationv3")
                .where("id = ?", data.annotation.annotation_id)
                .toString();
        var conn = getConnectionBySuffix(data.token.suffix);

        bddReqSocket(conn, request, '', 'updateAnnotationFeedback', insertAnnotation, data);
    };


    var insertAnnotation = function (result, socketClientId, title, data) {
        if (result.length != 0) {
           // console.log("supression error");
        }


        var origin_id;

        if (data.annotation.originType === "Projet") {
            origin_id = "projet_id";
        } else if (data.annotation.originType === "Convocation") {
            origin_id = "seance_id";
        } else if (data.annotation.originType === "Annexe") {
            origin_id = "annexe_id";
        }

        var sharedUsersId;



        if (!data.annotation.annotation_shareduseridlist) {
           // console.log("onto the if");
            sharedUsersId = JSON.stringify([]);
        } else {
           // console.log("into the else");
            notifySharedUser(data.annotation);
            sharedUsersId = JSON.stringify(data.annotation.annotation_shareduseridlist);
        }

        //stringify annotation if not string (from ios)
        if (typeof data.annotation.annotation_rect !== 'string'){
            data.annotation.annotation_rect = JSON.stringify(data.annotation.annotation_rect);
        }
        //prevent text to be null or undefined
        if (!data.annotation.annotation_text) {
            data.annotation.annotation_text = "";
        }



        try {
         var request = squel.insert()
             .into("annotationv3")
             .set("id", data.annotation.annotation_id)
             .set("authorId", data.annotation.annotation_author_id)
             .set("authorName", data.annotation.annotation_author_name)
             .set("page", data.annotation.annotation_page)
             .set("rect", data.annotation.annotation_rect)
             .set("date", data.annotation.annotation_date)
             .set("text", data.annotation.annotation_text)
             .set(origin_id, data.annotation.originId)
             .set("sharedUserIdList", sharedUsersId)
             .toString();

     }catch (e) {
         console.log(e);
         logger.error(e);
         freeQueue(null, null, null);
         return
     }

        var conn = getConnectionBySuffix(data.token.suffix);

        socketClientId = Clients.getSocketByClient(data.token.user_id);

        if (data.annotation.annotation_shareduseridlist && data.annotation.annotation_shareduseridlist.length > 0) {
            bddReqSocket(conn, request, socketClientId, title, updateSharedUsers, data);
        }

        bddReqSocket(conn, request, socketClientId, title, freeQueue, data);
    };




    var updateSharedUsers = function (result, socketClientId, title, data) {

        var annotUsers = [];
        data.annotation.annotation_shareduseridlist.forEach(function (userId) {
            annotUsers.push({"annotationv3_id": data.annotation.annotation_id, user_id: userId});
        });

        var request = squel.insert()
                .into("annotationv3_users")
                .setFieldsRows(annotUsers)
                .toString();

        var conn = getConnectionBySuffix(data.token.suffix);
        bddReqSocket(conn, request, socketClientId, title, freeQueue, data);
    };




    var freeQueue = function (result, socketClientId, title, data) {
        //set the queue status to not already running
        isRunningAnnotationQueue = false;
        //notify the client
        nsp.to(socketClientId).emit(title, "");
        //run the queue
        manageQueue()
    }
    
    //free the queue in case of database error
    var freeQueueError= function(title){
        if(title == "updateAnnotationFeedback" || title == "addNewAnnotationFeedback"){
             isRunningAnnotationQueue = false;
             manageQueue();
        } 
    }
        
    


//useless now
    var manageAnnotation = function (data, socket) {
        logger.debug("manageAnnotation");


        //todo TRY CATCH jsonexception  
        var annotations = [];

        try {
            for (var i = 0, l = data.annotation.length; i < l; i++) {
                annotations.push(JSON.parse(data.annotation[i]));
            }
        } catch (e) {
            logger.error(e);
        }



        return;



//TODO to it later
        /*        //if someone else try to modify or add annotation do nothing
         if (jsonAnnot.annotation_author_id != data.token.user_id) {
         console.log(data.token.user_id + "is trying to add, modify or delete annotation as " + jsonAnnot.annotation_author_id);
         return;
         }
         */



        var request = squel.select()
                .from("annotationv3")
                .where("id = ?", jsonAnnot.annotation_id)
                .toString();


        var conn = getConnectionBySuffix(data.token.suffix);

        //check if annotation already exists
        pg.connect(conn, function (err, client, done) {
            if (err) {
                return console.log('error fetching client from pool %s', err);
            }
            client.query(request, function (err, result) {
                //call `done()` to release the client back to the pool
                done();
                if (err) {
                    //TODO client should print this particular error !
                    nsp.to(socket.client.id).emit('authFeedback', {success: false, message: 'Authentication failed. database error.', token: null, userId: null});
                    return console.log(err);

                }

                var res = result.rows[0];

                //if anotation already exists update it
                if (res) {
                    updateAnnotation(data, socket);
                }
                //new annotation
                else {
                    insert([], socket.client.id, "addNewAnnotationFeedback", data);
                }


            });
        });

    };





    //useless now
    var updateAnnotation = function (data, socket) {
        logger.debug("update annotation");

        try {
            var jsonAnnot = JSON.parse(data.annotation);
        } catch (e) {
            logger.error(e);
            return;
        }

        var request = squel.delete()
                .from("annotationv3")
                .where("id = ?", jsonAnnot.annotation_id)
                .toString();

        var conn = getConnectionBySuffix(data.token.suffix);

        bddReqSocket(conn, request, socket.client.id, 'updateAnnotationFeedback', insert, data);

    };


    //useless now
    var insert = function (result, socketClientId, title, data) {

        if (result.length != 0) {

            return;
        }

        try {
            var jsonAnnot = JSON.parse(data.annotation);

            var origin_id;

            if (data.type == "Projet") {
                origin_id = "projet_id"
            } else if (data.type == "Convocation") {
                origin_id = "seance_id"
            } else if (data.type == "Annexe") {
                origin_id = "annexe_id"
            }

            var sharedUsersId;
            if (!jsonAnnot.annotation_shareduseridlist) {
                sharedUsersId = JSON.stringify([]);
            } else {
                notifySharedUser(jsonAnnot, data.type, data.originId);
                sharedUsersId = JSON.stringify(jsonAnnot.annotation_shareduseridlist);
            }

            //prevent text to be null or undefined
            if (!jsonAnnot.annotation_text) {
                jsonAnnot.annotation_text = "";
            }

            var request = squel.insert()
                    .into("annotationv3")
                    .set("id", jsonAnnot.annotation_id)
                    .set("authorId", jsonAnnot.annotation_author_id)
                    .set("authorName", jsonAnnot.annotation_author_name)
                    .set("page", jsonAnnot.annotation_page)
                    .set("rect", jsonAnnot.annotation_rect)
                    .set("date", jsonAnnot.annotation_date)
                    .set("text", jsonAnnot.annotation_text)
                    .set(origin_id, data.originId)
                    .set("sharedUserIdList", sharedUsersId)
                    .toString();



            var conn = getConnectionBySuffix(data.token.suffix);


            if (jsonAnnot.annotation_shareduseridlist && jsonAnnot.annotation_shareduseridlist.length > 0) {
                bddReqSocket(conn, request, socketClientId, title, updateSharedUsers, data);
            }
            bddReqSocket(conn, request, socketClientId, title, null, null/*verifAnnot, {id : jsonAnnot.id, conn: conn}*/);
        } catch (e) {
            console.log(e);
        }

    }




    var confirmPresence = function(data, socket){

        var request = squel.update()
            .table("convocations")
            .set("presentStatus", data.presentStatus)
            .where("user_id = ?", data.token.user_id )
            .where("seance_id = ?", data.seanceId)
            .toString()

        var conn = getConnectionBySuffix(data.token.suffix);
        bddReqSocket(conn, request, socket.client.id,'confirmPresenceFeedback', null, null );
     }



    var deleteAnnotations = function (data, socket) {
        logger.debug("deleteAnnotations");
        if(!data.ids || data.ids.length == 0){
            return;
        }
        //check if it's a shared annotations
        var request = squel.select()
                .from("annotationv3")
                .field("shareduseridlist", 'sharedUsers')
                .field("id", 'annotation_id')
                //.field("projet_id", "projet_id" )
                //.field("seance_id", "seance_id" )
                //.field("annexe_id", "seance_id" )
                .where("id IN ?", data.ids)
                .toString();

        var conn = getConnectionBySuffix(data.token.suffix);
        bddReqSocket(conn, request, socket.client.id, 'deleteAnnotationFeedback', deleteAnnotationsForReal, data);
    }



    var deleteAnnotationsConnect = function (data, socket) {
        //if someone else try to delete annotation do nothing
        /* if (data.authorId != data.token.user_id) {
         console.log(data.token.user_id + "is trying to delete annotation as " + data.authorId);
         return;
         }*/

        if (data.deleteOfflineList.length < 1) {
            nsp.to(socket.client.id).emit("deleteAnnotationsConnectFeedback");
            return;
        }

        data.ids = data.deleteOfflineList;

        //check if it's a shared annotations
        var request = squel.select()
                .from("annotationv3")
                .field("shareduseridlist", 'sharedUsers')
                .field("id", 'annotation_id')
                .where("id IN ?", data.deleteOfflineList)
                .toString();


        var conn = getConnectionBySuffix(data.token.suffix);

        bddReqSocket(conn, request, socket.client.id, 'deleteAnnotationsConnectFeedback', deleteAnnotationsForReal, data);
    };


    var deleteAnnotationsForReal = function (result, socketClientId, title, data) {
        /*result should be like this 
         [ { sharedUsers: '["55ed8758-cf7c-4893-b30c-3bb066d7706e"]',
         annotation_id: '45863e22-d803-482c-9f74-69df2b3c1a4c' ,
         //   projet_id: '45863e22-d803-482c-9f74-69df2b3c1a4c' ,
         //   seance_id : null,
         //   annexe_id: null} ]
         */
        //if the annotation is shared

        if (result[0] && result[0].sharedusers) {
           // console.log("NOTIFYUSERS");
            //notify shared users that the annotaion is remove
            notifyAnnotationDelete(result);
        }



        var request = squel.delete()
                .from("annotationv3")
                .where("id IN ?", data.ids)
                .toString();


        var conn = getConnectionBySuffix(data.token.suffix);
        bddReqSocket(conn, request, socketClientId, 'deleteAnnotationFeedback');
    };


    var notifyAnnotationDelete = function (data) {
        logger.debug("notifyAnnotationDelete");


        //for each annotation
        for (var i = 0, l = data.length; i < l; i++) {
            var annotation = data[i];
            //parse sharedUsers json array
            try {
                var jsonSharedUsers = JSON.parse(annotation.sharedusers);
            } catch (e) {
                logger.error(e);
                return;
            }
            //for each user
            for (var j = 0, la = jsonSharedUsers.length; j < la; j++) {
                //get the socketid for the client
                var socketClientId = Clients.getSocketByClient(jsonSharedUsers[j]);
                if (socketClientId) {
                    nsp.to(socketClientId).emit('sharedAnnotationDeleted', {annotation_Id: annotation.annotation_id});
                }
            }
        }
    };





    var sendAnnotationRead = function (data, socket) {
        logger.debug("sendAnnotationRead");
        if(!data.annotationIds || data.annotationIds.length == 0){
            return;
        }
        var request = squel.update()
                .table("annotationv3_users")
                .set("isread", true)
                .where("annotationv3_id IN ?", data.annotationIds)
                .where("user_id = ?", data.token.user_id )
                .toString()

        var conn = getConnectionBySuffix(data.token.suffix);
        bddReqSocket(conn, request, socket.client.id, 'sendAnnotationReadFeedback', null, null);
    }


    var readAnnotationsConnect = function (data, socket) {
        logger.debug("readAnnotationsConnect");
        if (data.readOfflineList.length < 1) {
            nsp.to(socket.client.id).emit("readAnnotationsConnectFeedback");
            return;
        }

        var request = squel.update()
                .table("annotationv3_users")
                .set("isread", true)
                .where("annotationv3_id IN ?", data.readOfflineList)
                .toString()

        var conn = getConnectionBySuffix(data.token.suffix);
        bddReqSocket(conn, request, socket.client.id, 'readAnnotationsConnectFeedback', null, null);
    };


    var notifySharedUser = function (annotation) {
        logger.debug("notifySharedUser");
        annotation.annotation_shareduseridlist.forEach(function (userId) {
            //get the socketId client
            var clientSocket = Clients.getSocketByClient(userId);
            //send notification to this client
            var toSend = {
                originType: annotation.originType, //redondant
                originId: annotation.originId, //redondant
                annotation: annotation
            };
            nsp.to(clientSocket).emit("newAnnotation", JSON.stringify(toSend));
        });
    };



    var getAnnotations = function (data, socket) {
        //TODO only on active seances !!!!!!
        var request = squel.select()
                .from("annotationv3")
                .field("annotationv3.id", "annotation_id")
                .field("annotationv3.projet_id", "annotation_projet_id")
                .field("annotationv3.seance_id", "annotation_seance_id")
                .field("annotationv3.annexe_id", "annotation_annexe_id")
                .field("annotationv3.authorid", "annotation_author_id")
                .field("annotationv3.authorname", "annotation_author_name")
                .field("annotationv3.text", "annotation_text")
                .field("annotationv3.date", "annotation_date")
                .field("annotationv3.page", "annotation_page")
                .field("annotationv3.rect", "annotation_rect")
                .field("annotationv3.shareduseridlist", "annotation_shareduseridlist")
                .field("true", "isread")

                .where("authorid = ?", data.token.user_id)

                .union(
                        squel.select()
                        .from('annotationv3_users')
                        .left_join("annotationv3", null, "annotationv3_users.annotationv3_id = annotationv3.id")
                        .field("annotationv3.id", "annotation_id")
                        .field("annotationv3.projet_id", "annotation_projet_id")
                        .field("annotationv3.seance_id", "annotation_seance_id")
                        .field("annotationv3.annexe_id", "annotation_annexe_id")
                        .field("annotationv3.authorid", "annotation_author_id")
                        .field("annotationv3.authorname", "annotation_author_name")
                        .field("annotationv3.text", "annotation_text")
                        .field("annotationv3.date", "annotation_date")
                        .field("annotationv3.page", "annotation_page")
                        .field("annotationv3.rect", "annotation_rect")
                        .field("annotationv3.shareduseridlist", "annotation_shareduseridlist")
                        .field("annotationv3_users.isread", "isread")

                        .where("annotationv3_users.user_id = ?", data.token.user_id)

                        )
                .toString();

        var conn = getConnectionBySuffix(data.token.suffix);
        bddReqSocket(conn, request, socket.client.id, 'getAnnotationsFeedback', formatAnnotations, data);
    }


    var formatAnnotations = function (annotations, socketClientId, title, data) {
        logger.debug("formatAnnotations");
        try {
            for (var i = 0, l = annotations.length; i < l; i++) {
                if (annotations[i].annotation_shareduseridlist != 'undefined') {
                    annotations[i].annotation_shareduseridlist = JSON.parse(annotations[i].annotation_shareduseridlist);
                } else {
                    annotations[i].annotation_shareduseridlist = null;
                }
                if(annotations[i].annotation_rect != 'undefined'){
                    // TODO works for ios test if work for android too !!!!!
                    annotations[i].annotation_rect = JSON.parse(annotations[i].annotation_rect);
                }
                
            }
            nsp.to(socketClientId).emit(title, JSON.stringify(annotations));
        } catch (e) {
            console.log(e);
        }
    };



    function ars(data, socket) {


        var dateWithStamp = new Date();
        var offset = (new Date().getTimezoneOffset() * 60 * 1000);
        var timeStamp = new Date(dateWithStamp.getTime() - offset).toISOString();
       // console.log(timeStamp);

        //if there are seances to set as read
        if (data.seancesId && data.seancesId.length > 0) {
            var request = squel.update()
                    .table("convocations")
                    .set("read", true)
                    .set("ar_horodatage", timeStamp)
                    .where("convocations.seance_id IN ?", data.seancesId)
                    .where("convocations.user_id = ?", data.token.user_id)
                    .toString();

            var conn = getConnectionBySuffix(data.token.suffix);
            bddReqSocket(conn, request, socket.client.id, 'convocationReadFeedBack', null, null);
        }
    }


    /**
     *
     * @param {type} data
     * @param {type} socket
     * @returns {undefined}
     */
    function updateSeances(data, socket) {
        logger.debug("updateSeances");

        //ask all seances
        var request = squel.select()
                .from("convocations")
                .join("seances", null, "convocations.seance_id = seances.id")
                .left_join("projets", null, "projets.seance_id = seances.id")
                .left_join("pthemes", null, "projets.ptheme_id = pthemes.id")
                .left_join("annexes", null, "projets.id = annexes.projet_id")
                .left_join("users", null, "projets.user_id = users.id")

                .field('seances.id', "seance_id")
                .field("seances.name", 'seance_name')
                .field("seances.rev", 'seance_rev')
                .field("seances.date_seance", "seance_date")
                .field("seances.place", "seance_place")
                .field("seances.document_id", "seance_document_id")
                .field("convocations.presentstatus", "presentstatus")

                .field("projets.name", 'projet_name')
                .field("projets.id", "projet_id")
                .field("projets.rank", "projet_rank")
                .field("projets.document_id", "projet_document_id")
                .field("projets.user_id", "projet_user_id" )
                .field("users.lastname", "projet_user_lastname" )
                .field("users.firstname", "projet_user_firstname" )
//               .field("pthemes.fullname", 'ptheme_name')
                .field("pthemes.name", 'ptheme_name')

                .field("convocations.read", "convocation_read")
                .field("convocations.id", "convocation_id")

                .field("annexes.id", "annexe_id")
                .field("annexes.name", "annexe_name")



                .where("convocations.user_id = ?", data.token.user_id)

                .where("convocations.active = true")
                .order("seance_id")
                .toString();

        //TODO CHECK if data

        var conn = getConnectionBySuffix(data.token.suffix);
        bddReqSocket(conn, request, socket.client.id, 'updateSeancesFeedback', formatgetAll, data.seancesList);
    }
    ;


    /**
     * set a convocation to read with userId and seanceId
     * @param {String} userId
     * @param {String} seanceId
     * @param {tString} socketId
     * @returns {void}
     */
    var setConvocationRead = function (userId, seanceId, conn, socketId) {
        logger.debug("setConvocationRead");
        var request = squel.update()
                .table("convocations")
                .set("read", true)
                .where("convocations.seance_id = ?", seanceId)
                .where("convocations.user_id = ?", userId)
                .toString();
        var conn = getConnectionBySuffix(data.token.suffix);
        bddReqSocket(conn, request, socketId, 'ConvocationReadFeedBack', null, null);
    };


    function archivedSeancesList(data, socket) {
        logger.debug("archivedSeancesList");
        var userId = data.token.user_id;
        var conn = getConnectionBySuffix(data.token.suffix);

        var request = squel.select()
                .from("convocations")
                .join("seances", null, "convocations.seance_id = seances.id")
                .field("seances.id", "seance_id")
                .field("seances.name", "seance_name")
                .field("seances.date_seance", "seance_date")
                .field("seances.document_id", "seance_document_id")

                .where("convocations.user_id = ?", userId)
                .where("seances.archive = true")
		.order("seances.date_seance",false)
                .toString();

        bddReqSocket(conn, request, socket.client.id, "archivedSeancesListFeedBack", formatArchivedSeance, data);
    }

    /**
     *
     * @param {type} data data.token, date.seanceId
     * @param {type} socket
     * @returns {undefined}
     */
    function archivedProjetsList(data, socket) {
        logger.debug("archivedProjetsList");
        var userId = data.token.user_id;
        var conn = getConnectionBySuffix(data.token.suffix);
        var request = squel.select()
                .from("seances")
                .left_join("projets", null, "seances.id = projets.seance_id")
                .left_join("pthemes", null, "projets.ptheme_id = pthemes.id")
                .left_join("annexes", null, "projets.id = annexes.projet_id")
  // unactive for test              .left_join("annotationv3", "annotationv3_projet", "projets.id = annotationv3_projet.projet_id and annotationv3_projet.authorid ='" + userId + "'")
                //.left_join("annotationv3", "annotationv3_projet", "projets.id = annotationv3_projet.projet_id")
                //  .left_join("annotationv3", "annotationv3_convocation" , "seances.id = annotationv3_convocation.seance_id and annotationv3_convocation.authorid = '" + userId+ "'" )


                //.field("seances.document_id")
                .field("projets.id", "projet_id")
                .field("projets.name", "projet_name")
                .field("projets.rank", "projet_rank")
                .field("projets.document_id", "projet_document_id")
                .field("pthemes.name", "ptheme_name")

                .field("annexes.id", "annexe_id")
                .field("annexes.name", "annexe_name")


/* unactive for test
                .field("annotationv3_projet.id", "annotation_id")
                .field("annotationv3_projet.projet_id", "annotation_projet_id")
                .field("annotationv3_projet.seance_id", "annotation_seance_id")
                .field("annotationv3_projet.annexe_id", "annotation_annexe_id")
                .field("annotationv3_projet.authorid", "annotation_author_id")
                .field("annotationv3_projet.authorname", "annotation_author_name")
                .field("annotationv3_projet.text", "annotation_text")
                .field("annotationv3_projet.date", "annotation_date")
                .field("annotationv3_projet.page", "annotation_page")
                .field("annotationv3_projet.rect", "annotation_rect")
                .field("annotationv3_projet.shareduseridlist", "annotation_shareduseridlist")
*/
                /*
                 .field("annotationv3_convocation.id", "annotation_id")
                 .field("annotationv3_convocation.projet_id", "annotation_projet_id")
                 .field("annotationv3_convocation.seance_id", "annotation_seance_id")
                 .field("annotationv3_convocation.annexe_id", "annotation_annexe_id")
                 .field("annotationv3_convocation.authorid", "annotation_author_id")
                 .field("annotationv3_convocation.authorname", "annotation_author_name")
                 .field("annotationv3_convocation.text", "annotation_text")
                 .field("annotationv3_convocation.date", "annotation_date")
                 .field("annotationv3_convocation.page", "annotation_page")
                 .field("annotationv3_convocation.rect", "annotation_rect")
                 .field("annotationv3_convocation.shareduseridlist", "annotation_shareduseridlist")
                 */


                .where("seances.id = ?", data.seanceId)
                .toString();

        bddReqSocket(conn, request, socket.client.id, "archivedProjetsListFeedBack", formatArchivedProjets, data);
    }








    /**
     *
     * request to database then send response to the socket client id.
     * optionnal function to parse data
     * @param {String} conn
     * @param {String} request
     * @param {String} socketClientId
     * @param {String} title
     * @param {function} traitement
     * @returns {void}
     */
    var bddReqSocket = function (conn, request, socketClientId, title, traitement, data) {
        logger.debug("bddReqSocket : ", {title: title, conn: conn});
        logger.silly("bddReqSocket : ", {request: request});

        pg.connect(conn, function (err, client, done) {
            if (err) {
                logger.error("bddReqSocket : connection error ", {title: title, conn: conn, error:err});
                freeQueueError(title);
                return console.log('error fetching client from pool %s', err);
               
            }
            client.query(request, function (err, result) {
                //call `done()` to release the client back to the pool
                done();
                if (err) {
                    logger.error("bddReqSocket : request error ", {title: title, conn: conn, error:err});
                    freeQueueError(title);
                    return console.log(err);
                }
 //apply function on result


                if(title == "userList"){
//                    console.log(result.rows);
                }
                if (traitement) {
                    traitement(result.rows, socketClientId, title, data);
                } else {
                    var response = JSON.stringify(result.rows);
                    logger.silly("bddReqSocket : send response ", {title:title, response: response, conn:conn});
                    nsp.to(socketClientId).emit(title, response);
                }
            });
        });
    };


    function formatArchivedSeance(seancesJson, socketClientId, title) {
        logger.debug("formatArchivedSeance");
        for (var i = 0, ln = seancesJson.length; i < ln; i++) {
            seancesJson[i].seance_date = seancesJson[i].seance_date.getTime().toString();  //toString add for ios
        }
        var response = JSON.stringify(seancesJson)
        nsp.to(socketClientId).emit(title, response);
    }
    


    function formatArchivedProjets(projetsJson, socketClientId, title) {
        logger.debug("formatArchivedProjets");

        var projets = [];
        for (var i = 0, ln = projetsJson.length; i < ln; i++) {
            //Check if the projet is already in the mapSeance
            var position = alreadyInProjet(projetsJson[i].projet_id, projets);
            //si le projet est déja présent on ajoute simplement l'annexe
            if (position > -1) {
                //add annexes
                if (getFormatAnnexe(projetsJson[i]).annexe_id) {
                    projets[position].annexes.push(getFormatAnnexe(projetsJson[i]));
                }

                //addAnnotation
                if (projetsJson[i].annotation_id) {
                    projets[position].annotations.push(getFormatAnnotation(projetsJson[i]));
                }
            } else {
                projets.push(getFormatProjet(projetsJson[i]));
            }
        }
        var response = JSON.stringify(projets);
        nsp.to(socketClientId).emit(title, response);
    }


    /**
     * Format the result from getall request and send it to the client
     * @param {JsonObject} seances
     * @param {string} socketClientId
     * @param {string} title
     * @returns {void}
     */
    var formatgetAll = function (seances, socketClientId, title, data) {
        logger.debug("formatgetAll");
        //format the data from database
        var mapSeance = {};
        for (var i = 0; i < seances.length; i++) {
            //si cette seance n'est pas deja dans la map
            if (!mapSeance[seances[i].seance_id]) {
                mapSeance[seances[i].seance_id] = formatSeance(seances[i]);
                //si elle est deja présente alors on rajoute le projet à cette seance
            } else {
                //on ajoute a la suite de celui qui a la meme id
                // si il ya  a desja un projet on l'ajoute a la suite
                if (mapSeance[seances[i].seance_id].projets) {
                    var position = alreadyInProjet(seances[i].projet_id, mapSeance[seances[i].seance_id].projets);
                    //si le projet est déja présent on ajoute simplement l'annexe
                    if (position > -1) {
                        mapSeance[seances[i].seance_id].projets[position].annexes.push(getFormatAnnexe(seances[i]))
                    } else {
                        mapSeance[seances[i].seance_id].projets.push(getFormatProjet(seances[i]));
                    }
                } else {
                    // sinon on le crée
                    mapSeance[seances[i].seance_id].projets = [].push(getFormatProjet(seances[i]));
                }
            }
        }

  for (var key in mapSeance) {
            //console.log(mapSeance[key]);
            if (mapSeance[key].projets) {
                var projets = mapSeance[key].projets
                for (var p = 0; p < projets.length; p++) {
                    if (projets[p].annexes) {
                        projets[p].annexes.sort(function (a1, a2) {
                            if (a1.annexe_name.toLowerCase() < a2.annexe_name.toLowerCase())
                                return -1;
                            if (a1.annexe_name.toLowerCase() > a2.annexe_name.toLowerCase())
                                return 1;
                            return 0
                        });
                    }
                }

            }
        }

        //sort the seances to send to devices
        var listSeancesDevice = data;
        var toAdd = [];
        var toDelete = [];
        var toModify = [];


        _.each(mapSeance, function (seance) {
            //if seances already into the device
            var posInDevice = findPostionInDevice(seance.seance_id, listSeancesDevice);
            //if in the device
            if (posInDevice > -1) {
                //check if same revision
                if (seance.seance_rev == listSeancesDevice[posInDevice].seanceRev) {
                    //then nothing to do
                    //delte from listseanceDevice
                    listSeancesDevice.splice(posInDevice, 1);
                } else {
                    toModify.push(seance);
                    listSeancesDevice.splice(posInDevice, 1);
                }
            } else {
                //if not in device
                toAdd.push(seance);
            }

        });

        var toSend = {'toAdd': toAdd, 'toModify': toModify, 'toRemove': listSeancesDevice}
        nsp.to(socketClientId).emit(title, JSON.stringify(toSend));
    };

    // id of the seance on the server, listSeance seance on the device
    var findPostionInDevice = function (id, listSeances) {
        logger.silly("findPostionInDevice");
        for (var i = 0; i < listSeances.length; i++) {
            if (id == listSeances[i].seanceId) {
                return i;
            }
        }
        return -1
    }




    /**
     * check if the project id already exist and return the position on the seances array
     * @param {string} value
     * @param {array} array
     * @returns {Number}
     */
    var alreadyInProjet = function (value, array) {
        logger.silly("alreadyInPRojet");
        for (var i = 0, ln = array.length; i < ln; i++) {
            if (array[i].projet_id == value) {
                return i;
            }
        }
        return -1;
    }





    /**
     * format the data in seances
     * @param {jsonObject} jsonSeance
     * @returns {jsonObject}
     */
    var formatSeance = function (jsonSeance) {
        logger.silly("formatSeance");
        var map = {};
        map['seance_id'] = jsonSeance.seance_id;
        map['seance_name'] = jsonSeance.seance_name;
        map['seance_rev'] = jsonSeance.seance_rev.toString();
        //map['seance_date'] = jsonSeance.seance_date.toString();
        map['seance_date'] = jsonSeance.seance_date.getTime().toString(); // add toString for ios
        map['seance_place'] = jsonSeance.place;
        map['seance_document_id'] = jsonSeance.seance_document_id;
        map['convocation_read'] = jsonSeance.convocation_read;
        map["presentStatus"] = jsonSeance.presentstatus;
        map['convocation'] = {
            document_id: jsonSeance.seance_document_id,
            isRead: jsonSeance.convocation_read,
            id: jsonSeance.convocation_id
        };

        map['projets'] = [];

        //TODO faire un if pour le projet au cas ou pas de projet
        if (jsonSeance.projet_id) {

            var projetUserID = null;
            var projetUserName = null;

            if(jsonSeance.projet_user_id && jsonSeance.projet_user_firstname){
                projetUserID = jsonSeance.projet_user_id;
                projetUserName = jsonSeance.projet_user_firstname + " " + jsonSeance.projet_user_lastname;
            }


            map['projets'] = [
                {
                    projet_id: jsonSeance.projet_id,
                    projet_name: jsonSeance.projet_name,
                    projet_rank: jsonSeance.projet_rank,
                    projet_document_id: jsonSeance.projet_document_id,
                    projet_user_id : projetUserID,
                    projet_user_name : projetUserName,
                    ptheme_name: jsonSeance.ptheme_name,
                    annexes: []
                }
            ];




        }

        if (jsonSeance.annexe_id) {
            map.projets[0].annexes.push(getFormatAnnexe(jsonSeance));
        }
        return map;
    };

    /**
     * format the datas in projets
     * @param {jsonObject} jsonSeance
     * @returns {jsonObject}
     */
    var getFormatProjet = function (jsonSeance) {
        logger.silly("getFormatProjet");
        //TODO si le projet n'existe pas (au cas ou on et crée d'abord le projet avec une annexe useless

        var map = {
            projet_id: jsonSeance.projet_id,
            projet_name: jsonSeance.projet_name,
            projet_rank: jsonSeance.projet_rank,
            projet_document_id: jsonSeance.projet_document_id,
            ptheme_name: jsonSeance.ptheme_name,

            annexes: [],
            annotations: []

        };


        if(jsonSeance.projet_user_id && jsonSeance.projet_user_firstname){
            map.projet_user_id = jsonSeance.projet_user_id;
            map.projet_user_name = jsonSeance.projet_user_firstname + " " + jsonSeance.projet_user_lastname;
        }

        console.log(map);

        //if annexe
        if (jsonSeance.annexe_id) {

            map['annexes'] = [{
                    annexe_id: jsonSeance.annexe_id,
                    annexe_name: jsonSeance.annexe_name
                }
            ];
        }


        if (jsonSeance.annotation_id) {

            var parseAnnotation_shareduseridlist = null;
            try {
                parseAnnotation_shareduseridlist = JSON.parse(jsonSeance.annotation_shareduseridlist);
            } catch (e) {
                logger.error(e);
                return {};
            }

            map['annotations'] = [{
                    annotation_id: jsonSeance.annotation_id,
                    annotation_author_id: jsonSeance.annotation_author_id,
                    annotation_author_name: jsonSeance.annotation_author_name,
                    annotation_text: jsonSeance.annotation_text,
                    annotation_date: jsonSeance.annotation_date,
                    annotation_page: jsonSeance.annotation_page,
                    annotation_rect: jsonSeance.annotation_rect,
                    annotation_shareduseridlist: parseAnnotation_shareduseridlist,
                    annotation_projet_id: jsonSeance.annotation_projet_id,
                    originId: jsonSeance.annotation_projet_id, //always projet for now
                    originType: "Projet"
                            //   .field("annotationv3_projet.seance_id", "annotation_seance_id")
                            //   .field("annotationv3_projet.annexe_id", "annotation_annexe_id")
                }];
        }

        return map;
    };





    var getFormatAnnotation = function (jsonSeance) {
        logger.silly("getFormatAnnotation");
        var parseAnnotation_shareduseridlist = null;
        try {
            parseAnnotation_shareduseridlist = JSON.parse(jsonSeance.annotation_shareduseridlist);
        } catch (e) {
            logger.error(e);
            return {};
        }
        return{
            annotation_id: jsonSeance.annotation_id,
            annotation_author_id: jsonSeance.annotation_author_id,
            annotation_author_name: jsonSeance.annotation_author_name,
            annotation_text: jsonSeance.annotation_text,
            annotation_date: jsonSeance.annotation_date,
            annotation_page: jsonSeance.annotation_page,
            annotation_rect: jsonSeance.annotation_rect,
            annotation_shareduseridlist: parseAnnotation_shareduseridlist,
            annotation_projet_id: jsonSeance.annotation_projet_id,
            originId: jsonSeance.annotation_projet_id, //always projet for now
            originType: "Projet"
        };
    };


    /**
     * format the data in annexes
     * @param {type} jsonSeance
     * @returns {jsonObject}
     */
    var getFormatAnnexe = function (jsonSeance) {
        logger.silly("getFormatAnnexe");
        return{
            annexe_id: jsonSeance.annexe_id,
            annexe_name: jsonSeance.annexe_name
        };
    };



    return nsp;





};
