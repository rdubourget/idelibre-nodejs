async function fonctionAsynchroneOk() {
    // équivaut à :
    // return Promise.resolve('résultat');
    return 'résultat';
}
//fonctionAsynchroneOk().then(console.log) // log "résultat"

async function fonctionAsynchroneKo() {
    // équivaut à :
    // return Promise.reject(new Error('erreur'));
return "ok";
    throw new Error('erreur');
}

var succ = function(toto){
    console.log(toto);
    console.log("success");
}

fonctionAsynchroneKo().then(succ).catch(err => console.log(err.message)) // log "erreur"