var config = {};

//salt value from cakephp (authentification)
config.salt = 'DYhG93b0qyJfIxfs2guVoqsdqkjsdklazekji';

//level of log (debug, info, warn, error 0 for no log);
config.logLevel = 'debug';

//listened port
config.port = 8080;

//pass phrase from cakephp to send push from php server
config.passPhrase = "passphrase";

// token password
config.secret = 'password';

//token lifetime
config.tokenTimeout = 86400; //one day

config.logPath = '/var/www/socketIdelibre3/tmp/logs/idelibreNode.log';
config.zipPath = '/data/workspace/zip/';


config.horodatage  = 'http://horodatage.services.adullact.org/opensign.wsdl';

config.version = '3.1.0';



config.ADMINISTRATEURS = '26b380d8-93b2-41e9-9f68-da98c55ccb30';
config.UTILISATEURS = '9f803280-eb21-46b3-bcec-74ffc4d2aab8';
config.ACTEURS = 'b2f7a7e4-4ab1-4d93-ac13-d7ca540b4c16';
config.INVITES = 'aa130693-e6d0-4893-ba31-98892b98581f';
config.ADMINISTRATIFS = 'db68a3c7-0119-40f1-b444-e96f568b3d67';

config.MASTER_DATABASE = "postgres:idelibre:idelibre@localhost:5432/idelibre_admin";

// tableau de correspondance suffixe connexion
config.conMap = {

};

config.suffixUuidMap = {
    "libriciel" : "5810a34f-c540-4ad1-9caa-189c5cde535b"
}


var fs =require('fs');

// load the databases connection from files
try{
    var content = fs.readdirSync(__dirname + '/Connections');

    var res = [];
    content.forEach(function (name) {
console.log(name);
        res.push(fs.readFileSync(__dirname + '/Connections/' + name, 'utf8'));
    });

    res.forEach(function (el) {
        var jsonConn = JSON.parse(el);
        var key = Object.keys(jsonConn)[0];
        config.conMap[key] = jsonConn[key];

    });
}catch(err){
    console.log(err);
}

//export config module
module.exports = config;






